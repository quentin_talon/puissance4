#include "sortie_entree.h"
#include <vector>
#include <iostream>
#include <string>
#include "jeu.h"
using namespace std;

int quiCommence(vector<string> noms) //retour 1 si c'est le joueur 2 qui commence
{
    if(alea())
    {
        cout << noms[0]<< " commence la partie." << endl << endl;
        return 0;
    }
    else
    {
        cout << noms[1] << " commence la partie." << endl << endl;
        return 1;
    }
}

void afficher_plateau(vector<vector<string>> plateau) //affiche le tableau à l'état actuel
{
	// Tableau de tableau, 6 lignes, 7 colonnes
	// Un espace entre chaque colonne
	for(auto lignes : plateau)
	{
		for(auto elements : lignes)
		{
			cout << "[" << elements << "]";
		}
			cout << endl;
	}
	cout << " 1  2  3  4  5  6  7 " << endl << endl;
}

vector<string> demander_nom() //retourne le nom du joueur avec son sigle
{
	vector<string> noms(2,"0");
	cout << "Nom du joueur 1 (O) : ";
	cin >> noms[0];
    noms[0] = noms[0] + " (O)";
	cout << endl;
	cout << "Nom du joueur 2 (X) : ";
	cin >> noms[1];
    noms[1] = noms[1] + " (X)";
	cout << endl;
	
	return noms;
}

bool recommencer() //permet de recommencer la partie sans redemander le nom des joueurs
{
	bool recommencer(false);
	char reponse('a');
	do {
		cout << endl << "Souhaitez-vous commencer une nouvelle partie (y/n) ? " << endl ;
		cin >> reponse;
	} while (reponse != 'y' and reponse != 'n');

	if (reponse == 'y')
	{
		recommencer = true;
	}

	return recommencer;
}
