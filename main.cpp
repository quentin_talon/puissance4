#include <iostream>
#include <vector>
#include <string>

#include "jeu.h"
#include "sortie_entree.h"

using namespace std;

int main()
{
	vector<string> noms(demander_nom()); //on enregistre le nom des joueurs
	
	do
	{
		jouer(noms);
	}while (recommencer());

	return 0;
}

/*
 * Projet développé par Pierre-Adrien et Quentin
*/
