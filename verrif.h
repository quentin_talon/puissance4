#include <vector>
#include <string>
using namespace std;

bool win_vert(vector<vector<string>> plateau, int num_joueur); //verifie si un certain joueur a gagné en verticale
bool win_horiz(vector<vector<string>> plateau, int num_joueur); //verifie si un certain joueur a gagné en horizontale
bool win_diag(vector<vector<string>> plateau, int num_joueur); //verifie si un certain joueur a gagné en diagonale
