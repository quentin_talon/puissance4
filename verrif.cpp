#include "verrif.h"
#include <vector>
#include <iostream>
#include <string>
using namespace std;

bool win_vert(vector<vector<string>> plateau, int num_joueur) //verifie si un certain joueur a gagné en verticale
{
	// Retourne true si le joueur 1-2 gagne par la verticale
	// Je dois parcourir ligne par ligne
	bool win = false;
	string joueur; // Cette string est à rechercher à toutes les colonnes
	if(num_joueur == 0)
	{
		joueur = "XXXX";
	}else if(num_joueur == 1)
	{
		joueur = "OOOO";
	}else{
		cout << "Erreur de verrification de win, parametre num_joueur" << endl;
	}
	
	for(size_t c = 0 ; c<plateau[0].size(); c++) // Je parcours 
	{
		string ligne = "";
		for(size_t l =0; l<plateau.size() ; l++) // Je crée une grande string de tous les caract de la colonne
		{
			ligne = ligne + plateau[l][c];
		}
		if(ligne.find(joueur)!=string::npos) //Si la suite existe, je sort que oui il a win
		{
			win = true;
		}
	}
	return win;
}

bool win_horiz(vector<vector<string>> plateau, int num_joueur) //verifie si un certain joueur a gagné en horizontale
{
	// Retourne true si le joueur 1-2 gagne par l'horizontale
	// Je dois parcourir ligne par ligne
	bool win = false;
	string joueur; // Cette string est à rechercher à toutes les lignes
	if(num_joueur == 0)
	{
		joueur = "XXXX";
	}else if(num_joueur == 1)
	{
		joueur = "OOOO";
	}else{
		cout << "Erreur de verrification de win, parametre num_joueur" << endl;
	}
	
	for(size_t l = 0 ; l<plateau.size(); l++) // Je parcours 
	{
		string ligne = "";
		for(size_t c =0; c<plateau[0].size() ; c++) // Je crée une grande string de tous les caract de la ligne
		{
			ligne = ligne + plateau[l][c];
		}
		if(ligne.find(joueur)!=string::npos) //Si la suite existe, je sort que oui il a win
		{
			win = true;
		}
	}
	return win;
}

bool win_diag(vector<vector<string>> plateau, int num_joueur) //verifie si un certain joueur a gagné en diagonale
{
	// L'idée est de créer une string de n chars
	// De vérifier si la substring existe
	bool win = false;
	string joueur; // Cette string est à rechercher à toutes les diagonales
	if(num_joueur == 0)
	{
		joueur = "XXXX";
	}else if(num_joueur == 1)
	{
		joueur = "OOOO";
	}else{
		cout << "Erreur de verrification de diag_win, parametre num_joueur" << endl;
	}
	// Je dois monter à droite tant que les 2 éléments existent.
	vector<vector<int>> init_droite ({{3,0},{4,0},{5,0},{5,1},{5,2},{5,3}});
	int ligne (0);
	int colonne (0);
	for(size_t i = 0; i<init_droite.size(); i++)
	{
		string chaine = "";
		ligne=init_droite[i][0];
		colonne = init_droite[i][1];
		while(ligne >=0 and colonne < 7)
		{
			chaine = chaine + plateau[ligne--][colonne++];
		}
		if(chaine.find(joueur)!=string::npos)
			win = true;
	}
	// Je dois descendre à gauche tant que les 2 éléments exsitent.
	vector<vector<int>> init_gauche ({{2,0},{1,0},{0,0},{0,1},{0,2},{0,3}});
	for(size_t i = 0; i<init_droite.size(); i++)
	{
		string chaine = "";
		ligne=init_gauche[i][0];
		colonne = init_gauche[i][1];
		while(ligne <6 and colonne <7)
		{
			chaine = chaine + plateau[ligne++][colonne++];
		}
		if(chaine.find(joueur)!=string::npos)
			win = true;
	}
	
	return win;
}
