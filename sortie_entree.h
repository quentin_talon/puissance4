#include <iostream>
#include <vector>
#include <string>
using namespace std;

int quiCommence(vector<string> noms); //retour 1 si c'est le joueur 2 qui commence
bool recommencer(); //permet de recommencer la partie sans redemander le nom des joueurs
vector<string> demander_nom(); //retourne le nom du joueur avec son sigle
void afficher_plateau(vector<vector<string>> plateau); //affiche le tableau à l'état actuel
