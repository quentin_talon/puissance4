#include <iostream>
#include <vector>
#include <string>
#include "jeu.h"
#include "sortie_entree.h"
#include "verrif.h"
using namespace std;

bool alea() //retourne aléatroirement 0 ou 1
{
	srand((unsigned int)time(NULL));

	if (rand() % 2 == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool ajouterElement(vector<vector<string>>& plateau, bool num_joueur, int num_colonne) //modifie le plateau en fonction de la colonne et du joueur
{
    string sigle("O");
    if (num_joueur == 1)
        sigle = "X";
    
    int ligne(0);
    while (ligne < 6 and plateau[ligne][num_colonne-1] == " ")
    {
        ++ligne;
    }
    if (ligne == 0)
    {
        return false;
    }
    
    plateau[ligne-1][num_colonne-1] = sigle;
    return true;
}

void jouer(vector<string> noms) //enclanche et opère la procédure de jeu
{
    vector<vector<string>> plateau(6,vector<string>(7," ")); // on crée un plateau vide
    bool num_joueur(quiCommence(noms)); // On désigne aléatroirement un joueur pour commencer (le numéro est un bool car c'est soit l'un soit l'autre)
    int conteur(0); //On initialise le nombre de coups de la partie
    int num_colonne(0);
    bool bon_type;
    
    do
    {
        afficher_plateau(plateau); //on affiche le plateau dans son état actuel
        do
        {
            do // Tant que le joueur entre un numéro de colone incorrecte ou un numéro de colone pleine, on redemande
            {
				bon_type = true;
                cout << "C'est à " << noms[num_joueur] << " de jouer choisir une colonne : ";
                cin >> num_colonne;
                //*
                if(cin.fail()) //cin.fail() checks to see if the value in the cin
					//stream is the correct type, if not it returns true,
					//false otherwise.
				{	
					cin.clear(); //This corrects the stream.
					//cin.ignore();
					cin.ignore(100,'\n'); //This skips the left over stream data.
					bon_type = false; //The cin was not an integer so try again.
				}//*/
				//Fonctionne mais je sais pas pourquoi (Quentin)
				cout << endl;
            }while (num_colonne < 1 or num_colonne > 7 or !bon_type);
        }while (!ajouterElement(plateau, num_joueur, num_colonne));
        
        // on change de joueur pour le coup suivant
        num_joueur = (num_joueur+1)%2;
        ++conteur;
        
    } while (!win_diag(plateau, num_joueur) and !win_vert(plateau, num_joueur) and !win_horiz(plateau, num_joueur) and conteur < 42);
    
    // la partie est finie! on affiche les résultats
    afficher_plateau(plateau);
    if (conteur != 42 and (win_vert(plateau, num_joueur) or win_horiz(plateau, num_joueur) or win_diag(plateau, num_joueur)))
    {
        cout << endl << noms[!num_joueur] << " a gagné la partie";
        
        if (win_vert(plateau,num_joueur))
        {
            cout << " dans la colonne " << num_colonne;
        }
        
        if (win_horiz(plateau,num_joueur))
        {
            cout << " à l'horizontal";
        }
        
        if (win_diag(plateau,num_joueur))
        {
            cout << " en diagonale";
        }
        
        cout << " en " << conteur << " coups !" << endl;
    }
    else
    {
        cout << endl << "Vous avez fait ex-aecquo !" << endl;
    }
    plateau = vector<vector<string>> (6,vector<string>(7," ")); // on recrée un plateau vide
}

