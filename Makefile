CXX = g++-4.7

CXXFLAGS = -std=c++11 -Wall -c
LDFLAGS =
EXEC = main

# rm -f *.o
all: $(EXEC)
	@echo fin
$(EXEC): main.o verrif.o sortie_entree.o jeu.o
	$(CXX) -o $@ $^ $(LDFLAGS)

%.o: %.cpp
	$(CXX) -o $@ -cpp $< $(CXXFLAGS)

#executé si on fait make clean
clean:
	rm -rf *.o
mrproper: clean
	@rm -rf $(EXEC)
run: $(EXEC)
	@./$(EXEC)
